Rudra has disobeyed Bane in a direct order, he will be punished for this
> => He will dream of the fields of the dead, he stands alone on the foggy grasslands surrounded by the skulls of goats, there are hundreds around his feet.
> a lone figure (bane) strides forward out of the fog towards him. Bane says "You disobeyed me? ME? I SHALL SHOW YOU THE COST OF DISOBEYANCE, I SHALL SHOW IT A THOUSAND TIMES, you will either submit or show that you are worthy of my fists"
> Regardless of what Rudra answers, bane will then proceed to stab rudra with his spear, ripping open his belly, bane will put his hands inside rudras body and rip out his spine. Rudra will feel everything.
> After this rudra will wake up again and again inthe same landscape, bane will use many ways to kill rudra, but never use his fists to strike him first.
> Rudra will need to make a wisdom save to endure, if he fails he will wake up having lost Banes favour, if he endures he will gain banes appreciation.
> If rudra succeeds he will wake up a final time (this time the number of skulls is high enough to form a mountain {they are all his skull}) , he will be on his hands and knees, in the skulls he will see a slight glimmer of light, Myrkul lord of bones is watching.
> Bane will grab rudra by his horns and beat him to death, he will shout "YOU SEE WHAT IT TAKES TO BE A TYRANT, YOU MUST DO THE SAME TO YOUR ENEMIES"
> after this rudra will wake up, remembering the thousands of times he died at banes hands.

---
