f1) party fights and detains amrick
   - the party fights the bone devil and kill it, in the process drell spawned a statue clone of himself (from bag of beans), amrick is out cold, drell gathers some "remains" from the bone devil
     >![[session19_bag-of-beans-statue.png]]
   - they take amrick inside drells apartment and bind him there, there is a brief encounter with one of Drell's neighbors "Robin" but they shoo him off 
   - Faelyn threatens amrick with her rapier (by holding it against his troat), rudra does a little torturing (acid hands on amrick hands), they want to know various information (black gem {from drow's quest} + cult of tiamat location)
    > -> Amrick tells Faelyn that his mother keeps many artifacts, that he doesn't know
    > -> he tells the party that he has dealings with the cult of tiamat and that one of his contacts (a big balding man) will go to the meeting location for transactions (it is close to the basilisk gate, not far from drell's apartment, in a cellar)
2) rudra go gets mortlock
   - mortlock is at the inn, he follows rudra and aster back to drell's house, rudra does not destroy the statue that is screaming vile "facts" about some guy named drell that is nearby, some people watching from their windows
3) mortlock talks to the party
   - mortlock warns the party that his mother is powerful and will know that amrick is here if they don't deal with him somehow, he asks if he can kill amrick
   - aster asks about thavius krieg, mortlock answers that he is currently staying at his mother's mansion
   -  party is split over whether they should give amrick to his brother or to try to hostage him to his mother for leverage
   - in the absence of a proper hiding spot for amrick they decide to give him to his brother
4) shit ensues
   - mortlock pays rudra da money, and then proceeds to torture his brother to death
   - in the mean time the party argues over the money
   - outside people are gathering around the statue, a lot of attention is slowly gathering to drell's house as Robin tells his neighbors about the strange people he saw
   - while people are bonking on the door and mortlock tears of his brother's head with his bare hands rudra chooses to pay the rest of the party their fair share of the money
   - Mortlock picks up his brother's corpse (leaving his intestines on the floor) and tells the party that they should run
   - the party runs away, mortlock following them, there is a brief chase when a few flaming fist spot the shady figures running away from a suspicious scene (+ the statue is currently screaming that the child molesting, murderous goblin "Drell the Evil" is running away)
   - rudra hides and waits on a shaded roof untill everything blows over
5) party is now in mortlocks secret dungeon (except rudra)
   - suprise! mortlock does have a hiding spot after all, and a warded one at that, he tells the party to follow him there
   - mortlock leaves to "deal" with his brother's corpse
   - the party rests

---
