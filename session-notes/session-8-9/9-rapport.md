1) both team rudra and team Faelyn go towards area D12, fighting the fight there and somehow surviving
>    - before this theodore looted the sarcophagus in D11, getting briefly eaten by the bag of devouring (which rudra now has) and also taking the grimoire and potion of longevity
>    - Rudra got a vision when he exited area D10 from myrkul, he was commanded to kill every follower in the dungeon
   > - Rudra was commanded by bane to kill the captured person there, and to stop hiding... Rudra did that by casting spiritual guardians and also damaging his party members.
   > this ofc angered everyone, so he had to weasel out of the situation (the party now mistrusts him and will attack him the next time he steps out of line like this...)
   >  > --> Felix did not like this, he is starting to find the 3 too demanding (I have to find a way to reward him as-well ig)

2) with the party back together they go back to the myrkul's altar to short rest
   > - while they do, theodore and elvis went to research their grimoir's, elvis managed to get cursed an went berserk trying to kill drell, the party stopped him and removed the curse by removing the dagger he was holding
   
3) at the end of the short rest they are confronted by a figure at the door
   > - flennis has healed herself by absorbing the essence of the man on the table that elvis stabbed, she convinced the party to let her pass in turn for leaving her alone to leave, she tried to get her grimoire back by leaving one of her hands to steal it, it just failed (after many shitty rolls of the party holy hell)
   
4) the party leaves area D11
   > -> They venture down towards area D20, then everyone except drell and elvis ventures towards area D22
   > > --> drell and elvis explore area D20, they find all the loot, drell conflicts sewer plague by touching the sludge in the sarcophagus
   > 
   > -> in D22 the party frees vendetta kress from her torturer and abusive partner, she is suspicious but hopeful, she tried to save her partner because she didn't know who the party was and didn't want to trow away all her work over the past few months (even if it was out of desperation)
   
5) the party goes to the rests of the dead three
   > -> they take vendetta out of the torture room (after healing her bleeding wounds) as well as effinax
   > > --> Faelyn wants to question effinax, Rudra wants to keep him silenced. Faelyn removes his gag and Effinax uses the opportunity to warn the others of intruders before being killed by faelyn
   > > > => the others were warned and combat begins, the shade oneshots Rudra by sneak attacking him, after this session ends

---

