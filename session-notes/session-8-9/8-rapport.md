
The party is now fully in the [[dungeon-of-the-dead-three]], split into 2 groups:
> - Team Rudra (Rudra, Drell, Theodore and Aster)
>   > -> They started combat in D7, managing to beat Vistaak, after this they made their way down to D9, passing D8 (they find out it is a kind of diagram that is meant to depict a ritual scene , in particular one done to denote one done in thanks to bhaal, the mural serves as a guide to other followers), They entered D9 and found that the doors would not open (except the one leading to myrkuls chamber), they enter that chamber and do the fight, Rudra refuses to take part and Aster actually fully dies => Drell manages to save her by using his shard of chaos to mimic the revivify spell (succeeding the DC of 15) this way he manages to save asters life
>   > > --> Because Rudra did not take place in the fight and one of his companions died he succeeded the test of Myrkul, opening Banes Door
>  
> - Team Faelyn (Faelyn and Elvis) 
>    > -> Elvis was thrown into a cell with Faelyn (who is unconscious), he manages to escape by using phantasmal force on the guard and then shooting him, Elvis then used reduce on Faelyn so that he could carry her untill she woke up, they went up towards area D18 (where Faelyn woke up cuz the stink from the gas).
>    > they went to look through the door towards D12 but then decided to down towards D17 instead => Elvis found the words and said the prayer, giving him 3 skeletons to command
>    > with these in toe and faelyn awake they ventured further until they found D13, elvis used disguise self to mimic the hood of one of the cultists and entered the room, Flennis the necromancer was more than receptive towards him, showing off his work and his experimental knives
>    > Elvis stabbed flannis in the back starting a fight that downed him twice and nearly killed faelyn, the necromancer went unconcious but was not dead yet, they quickly looted the knives (the skellies held them) and the grimoire, team faelyn barely survived at very low hp

---
