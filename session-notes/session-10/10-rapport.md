1) the party fights the chosen within the rests of the dead three
   > - Rudra gets a vision from bhaal, healing him and giving him all long-rest benefits (after the fight)
2) they short rest in (and loot) the rest of bane
   > - the loot they find is:
   >   > 1) 3 chests filled with 50 gp 100sp and 75 gp worth of gems
   >   > 2) 1 chest filled with 6 health potions, of which 2 are used to resuscitate Faelyn and  Drell
   >   > 3) 1 chest filled with gauntlets
3) they go up towards area D29 through the hidden door
   > - elvis enters area D28, he gets instantly grappled and swarmed by the skellies there, party rescues him, they open the box and find the ampule, knife and the letter
4) they fight Vaaz, primate of bhaal of the cult of the dead three together with Mortlock and win

---
