1) players were told to meet up with a spy "Tarina" at the last lantern inn
 > Tarina is doing a job for the flaming fist, namely trying to infiltrate/keep an eye on the various cults that are going around the city, she recently managed to come into contact with multiple people from the cult of the dead three.
>>  --> They told her to meet at the =="Aquamarine bathhouse"== near the wide (market)

2) Players go to last lantern
> - **Aster** announced that they were trying to find Tarina in front of the whole last lantern inn (sparking some interested ears)
> > --> she also met (before entering inn, on top deck) a disguised hellrider that fled to Baldurs Gate after the fall of elturel, she is hiding because of the new order that all hellriders are suspect (drel gave symbol to captain, might have been too fast with doing the hiding bit, drel only showed it +- an hour ago). The hellrider recognizes aster from the church of Lathander, but aster does not (failed insight), she tells **aster** to ==meet up in the evening at this inn==
> - **Faelyn** met up with some interesting characters, one of whom (scarred and hooded, sharp teeth {unsure to make him vampire or something}) could smell that she came from the shadowfell and asked her questions about that, in return for her answers he told her Tarina was downstairs and wearing a hat
> > --> **Faelyn** also caught the eye of a drow man that started following them invisibly after they left the inn (players DO NOT yet know!) ==> he is following because he is interested in the shadar-kai, namely their ability to be reborn, he thinks that studying their blood will unlock something similar in himself
> - **Drel** met a duegar whom he owes 20gp but managed to persuade him from pursuing it
> - **Elvis** joined in a game of dice with the duegar, he lost
> - **Theodore** met a dwarf that was traveling from the city of splendors (waterdeep) to athkatla to make his fortune, he is currently stranded in baldurs gate because of the lockdown (from fall of elturel) --> he is a trader but players didn't inquire further so whatever?
> > --> he also met a tiefling mage girl (with her crab familiar), she told him the names of the 9 layers of the hells and pointed him to the =="Devil's Fee"==, saying that he might find a way to get into the hells, but that he needs to be prepared to pay a steep price --> the mage girl is a student of a magelord of waterdeep, she is currently staying in baldurs gate as she was traveling to candlekeep (and went by foot cuz seeing things/traveling is nice) players wont inquire further ig
> - **Rudra** did not do much within the inn at this stage

3) Players confront Tarina
> - Aster already exposed Tarina by asking for her when she entered inn, party asked nearly everyone in inn about her, aster also showed the badge of the flaming fist publicly to Tarina (she did not try to hide her), nearly entirely blowing her cover or at least making her suspect to the cults (cult of dead three member saw party do it)
> > ==> they loudly confront Tarina, demanding to know all she knows
> > > --> she tells them the following:
> > > > - the cult of the dead three is likely causing the majority of the kidnappings in the heap, they are targeting people at random and used the cover of the distraction of the flaming fist to increase their activity, she tells the party to go to the aquamarine bathhouse as that is where she was to meet the cult
> > > > - there is a killer (Rudra) murdering people at random, mostly in the heap distict, the party tells Tarina of the symbol (the sun of elturel) but she dismissed the theory of the hellriders being responsible as the murders started too soon after the fall, unless of course, the hellriders were planning this before or had some other goals
> > > > - Another cult is kidnapping children and teens in the steaps district, they have been very difficult to track

4) players rescue Tarina
> - The players escort Tarina to safety cus she is suspect and thinks she will be in danger if she doesn't flee, Faelyn notices that they are being followed, she tries to give chase but loses the hooded figures in an alley
>> --> the party gets back to the captain without problems

5) Players discuss plans and choose their path
> - In Drell's house they discuss the information they were given

---
