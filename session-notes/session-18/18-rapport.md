1) the party goes shopping
>    -> Faelyn buys a +1 rapier from Zalalazars  magical goods for 300 gp
   > -> elvis  goes back to the brewing potion to ask about dust, the proprietor has found some since the morning (crystal dust, still need to make the item...)
   > -> Rudra buys and then returns a piece of rough plate mail, he is not welcome in that armorer again (shop had a dwarf named baldrin, it was baldrins metal-works)
   
2) party goes to [[The Unrolling Scroll]] to buy magic items
   > -> Theodore gets into a vision of inspiration granted by [[Oghma]] to those devout that stare into the mirror pool at the centre of the shrine, aster pulls him out of it before he gains the blessing
   > -> Theorodore and the rest of the party ask the scroll-keeper of Oghma for information about avernus, and about the book and how to open it again, she tells him he needs soul coins to open the book
   > -> rudra is briefly cornered about his affiliation because the following of he who knows sees his god's mark on him, she lets him escape by letting him tell his "secret", he tells the party that he killed his own mother, tho he does not remember why or how
3) party plans the attack on amrick in the inn
   > -> the party goes to the last light inn where they discuss how to get to amrick, they descide to disguise Elvis as a prostitute to lure Amrick outside
   > -> After a lot of trying (and alterself + disguise self + a good insight roll = very bootyfull) Elvis gets Amrick to follow him outside to drell's home
4) Party fights Amrick
   -> the party prepares an ambush on amrick at drell's home, they surprised him but amrick had some tricks up his sleeve, he almost got away but Faelyn knocked him unconscious

---