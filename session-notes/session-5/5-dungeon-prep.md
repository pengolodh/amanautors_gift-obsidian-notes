the party will likely (or not) enter the dungeon, the [notes](https://5e.tools/adventure.html#bgdia,1,dungeon%20of%20the%20dead%20three)will give you the general layout
> I will do the following changes:
> > - D3: south massage room
> > > => jabaz is a women, she is doing the whole massage business as a side job until she can afford to go to either waterdeep or candlekeep (she wants to learn magic, she hates her current job but likes some of the people she has met), her boss (Mortlock Vampathur) is an enormous asshole that pushes her to do things she does not want (not yet sexual, but close). Jabaz knows that Mortlock does strange things in the bathhouse during the evening, Mortlock warned her not to come close to the area of the bathhouse or the heap in the evening/night. Jabaz knows the location of a secret door in the other massage room, she is curious of what lies beyond it but has never dared enter the area, she will tell the party if they offer her a reason (and promise never to tell her boss she told them)
> 
> > - D4: north massage room
> > > => Qurmilah is an older lady (around 50), she does not care about the secret door, she knows its there but thinks its none of her business, if she massages a party member they need to make a con save cuz she got some strong forearms. Mortlock hired her because he knows she does not care about his activities (she worked as a prostitute in her earlier life) and will keep her nose out of his business, he mostly leaves er alone.
> > > ==> the secret door takes a dc 15 saving throw to find
> 
> > - D6: room near altar (after secret door)
> > > =>  the room is mostly empty exept for a trail of entrails leading into the next room
>
> > - D7: first shrine to bhaal
> > > => Vistaak is standing in prayer in front of the alter, when the party enters he is carefully adorning the alter to the god of murder with a victims corpse. 
> > > If the players choose to watch instead of immediately attacking they will see him carefully placing the eyeballs of the remains into the sockets of the skull of bhaal.
> > > Most of the body is placed on the alter underneath the skull, the entrails go around the skull, the head is placed in the mouth of bhaal.
> > > Vistaak will only acknowledge the players when he is done with his ritual. When the ritual is completed the eyes will lite up in red flames and fill the sockets.

---
