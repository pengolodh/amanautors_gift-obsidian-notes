Party has been given the following options to visit:
> - The Aquamarine bathhouse
> > --> This is where the dungeon of the dead three is located, the party will be welcomed to the bathouse, it will cost 12 silver a person for an hour, 2 gold if you want an included massage, 2.4 gp to 15 gp if you want some "company" (company  is a women or man to talk to/flirt with, price is dependant on lvel of service)
> > > ==> the party can choose to relax or interrogate the people working there for clues about the cult, depedning on the violence by which they conduct themeselves the cult might determine them to be at treat.
> > > > ---> if the party goes down into the dungeon follow the [notes](https://5e.tools/adventure.html#bgdia,1,dungeon%20of%20the%20dead%20three) about the dungeon, unless you wish to change anything specific (add that to [[dungeon-of-the-dead-three]])
> - The devils Fee shop
> > --> the propietor is a servant of Mephistopheles, archduke of Cania, 1 of the layers of  the hells, she can offer the party a ritual to get into avernus, it will cost them 10000 gp or 20000 gp if they want to visit a more dangerous location, the party will need to figure out how to conduct the ritual (see bg3 devils fee ritual)
> - Sorcerous Sundries magic emporium
> > --> Loroakan is arrogant, he will not talk to the party unless they are worth his time, he will deny meeting them unless they use the badge the captain gave them. even so he will not help them unless they do a task for him
> > > ==> He will tell them to find information for him, the information he seeks pertains to a relic named the "nightsong", he will tell the party to investigate this, he does not care how so long as they return with more information. If the party is successfull he will help them find their way into avernus
> - Helping the church of Ilmater with slum reparations/healing
> > --> Elvis needs to aid the church on order of the captain, if he does not he will be treated as the primary suspect of the murder cases and trialed accordingly
> > > ==> he needs to help those that fell without home, search for corpses in the burned rubble and aid in rescuing belongings if they survived

---

