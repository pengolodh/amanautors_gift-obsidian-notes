The party chose to go the devil's fee first
--> on the way to the devils fee they went through "the wide" market square
> - Faelyn acquired a new set of custom fitted black studded leather armor at a leather-workers shop on the market (there was a giant that used magic to fit her armor for an extra fee), she ran of with the armor (stealing it), it was worth 125 gp +/-
> - Elvis met an old potion selling women (auntie Ethel, a green hag) she sold him a vial of Purple dust (Glimmerdust) and another vail of something unknown
> > --> the unknown vail is a potion that turns a dust user into a michonid immediately when their stage of progression is in the last 2 levels, effectively restoring them to their sentient selves, if it is ingested before the dust is used however, it will take a longer time to turn the user, sprouting mushrooms out of their skull in a very painfull process. When turned the user is immune to the negative effects of dust. (might revise that)
> 
> - Elvis and the rest of the party went to the devils fee, faelyn was held up by the fitting process of her amor

==> meanwhile at the devils fee the party met with Helsik, the old night hag (in disguise) proprietor of the shop, she was very interested in taking samples from both Theodore and Rudra, since she has never seen there races before.
> - the party asked her for a way into the hells and information over the hells, she scoffed at them since such knowledge is very expensive (and the ingredients too) she quoted them 5000 gp to get into avernus with no particular destination.
> > --> the party could obviously not  pay this price so they searched for a way to sweeten the deal, Helsik said she would give them a steep discount if Theodore gave her a tissue sample to study. 
> 
> > ==> Theodore also offered her the (by now half empty) necrichor bottle he had been keeping in his pack, this was enough for her to give them what they asked for.
> 
> Helsik gave Theodore 3 things in exange for the Necrichor bottle and a tissue sample:
> > 1. The book: "Avernus: the mustering ground of the hells"
> > > -> the book is burning hot to living creatures, it details the plane of avernus as it was around 250 years ago (before Zariel), it goes in to a lot of detail on the politics and cultures of avernus and also the blood war, it details some of the biggest landmarks and describes some of the war machines used by the avernus devils.
> > 2. The book: "The nine hells, a detailed account"
> > > -> book with only 9 pages, it is bound in Imp skin that seems to still be alive (a pulse can be felt). each page describes one of the layers of the hells, a succesfull arcane check can be done for each page to uncover more information (ramping up to max dc of 20)
> > 3. The recipe for an infernal transposition ritual (to construct a temporary hellgate)
> 
> > -> she also sold theodore a napkin that absorbs heat (so that he can safely store/handle the book without burning himself) and a random trinket to Elvis
> 
> --> Elvis tried to steal something from the store, he failed all his rolls and got a face full of a strange powder instead, this blew him backwards and cursed him with -5 STR (can be healed by visiting a cleric of a high enough level)

---
