1) Theodore tries to talk to leroykon, 
> leroekon does not want to talk to people without explicit reason, he will deny all attempts to speak unless they are either related to the patriars or the flaming fist, or there is an emergency, he has however, already setup the communication channels to the relevant people for both these things, so he will not pay attention to people trying to contact him through sorcerers sundries.
> > -> he will pay attention if theodore **offers him knowledge of an artifact or magical ritual**

2) Elvis needs to help the temple of ilmatar fix the issue in the heap
   > -> they will make elvis attone for his mistake by letting him help fix the heap
   
3) Rudra gets his reward
   > -> if rudra sleeps outside of eyes reach, bhaal will give him his reward in the form of a dream of his youth, and the truth that he killed his own family when he was young, that bhaal was part of him from the beginning. Rudra will dream of scenes of slaughter and murder, he will see the truth of what happened to his "adopted family", where he grew claws and rended and tore them appart like paper. 
   > > He will awaken with claws and flesh. a slayer, the slayer form has the same statblock as a spined devil, he gets 2 levels of exhaustion when shifting back to his normal form, and everyone that sees the slayer will attack it, as it is an abomination, if he attempts to stay in the slayer form for longer than 20 seconds he takes damage as his soul is torn appart from the unconstrained power, he will take progressive damage the longer he stays in the form
   > > > - first 10 true damage
   > > > - after 10 extra seconds he takes 20 true damage
   > > > - then 40 true damage
   > > > - then 80 and so on...
   > > if he takes enough damage from this he reverts back to his normal form, this form will inherit the overdamage taken for 