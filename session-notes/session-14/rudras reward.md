He will awaken with claws and flesh. a slayer, the slayer form has the same statblock as a spined devil , he gets 2 levels of exhaustion when shifting back to his normal form, and everyone that sees the slayer will attack it, as it is an abomination, if he attempts to stay in the slayer form for longer than 20 seconds he takes damage as his soul is torn appart from the unconstrained power, he will take progressive damage the longer he stays in the form
   >  - first 10 true damage
   >  - after 10 extra seconds he takes 20 true damage
   >  - then 40 true damage
   >  - then 80 and so on...
   > 
   > if he takes enough damage from this he reverts back to his normal form
   
   