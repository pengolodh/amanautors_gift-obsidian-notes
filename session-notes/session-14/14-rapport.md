1) Theodore and Aster visit leroekon
   > -> theodore buys a few items in sorcerous sundries, namely: a hat of wizardry (150) and a spell scroll of enlarge/reduce (300gp) 
   > **(refer to [this](https://docs.google.com/spreadsheets/d/1xckMUATltAexbI6H5JVd1sFagaxjto78wV1alj3WUwE/edit#gid=308249492) spreadsheet, as well as [this](https://www.reddit.com/r/DnDBehindTheScreen/comments/uiwo1n/magic_items_pricing_guide_for_5e_a_spreadsheet/) post for pricing of other items in shop)**
   > -> they then manage to get an appointment with leroekon by showing aster's "flaming fisting badge" 
   > > => They talk to leroekon, asking him about avernus and telling him why they are there. 
   > > 
   > > He didn't have much information about avernus, but did give theodore a book about the beast-lands in exchange for his book of the 9 hells (the impskin one), theodore also told leroekon about the infernal transportation ritual he got from the devil's fee, for some reason I cannot fathom. 
   > > Leroekon, of course being very interested in this rare and powerful ritual, "steals" it immediately, well after giving theodore 200gp and a 100gp reduction ticket for his shop.
   
2) Rudra wanders around, visits the devil's fee to talk to the owner about his gloves
   > -> shop owner knows what rudra is from his blood samples, and recognizes the gloves for what they are, she does not tell him of a clear way to get rid of them. 
   > -> Rudra also asks her about the cult of tiamat, she gives him the information in exchange for his potions of fire breathing. She tells him about the lair of tiamat and some of the cult structure/enemies (from the forgotten realms [wiki](https://forgottenrealms.fandom.com/wiki/Church_of_Tiamat) page on the church of tiamat) like bahamut.
   
3) Elvis and Drell go to the heap to fix their problems, part 1
   > -> they meet with a priest of ilmater, who tells them to explore the heap and see the problems they caused.
   > -> they see a lot of suffering, drell gets kicked in the face becuase elvis told a person to do so
   > -> they return to the tent to report on what they had seen
   
4) Faelyn does nothing
   
---
