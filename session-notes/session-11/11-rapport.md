1) the party loots all the items specified in [[dungeon-of-the-dead-three]] D30 tot D33
> => Rudra finds a spear, a shield and a cloak in the covenant of the dead three (D33)
> > -> he saved his wisdom on all three
> > > - The **spear** forces you to obey bane, if the player fails 2 (dc 15 and dc20 wis) times it will permanently! force a geas onto the character ( this effect can be cleansed by a high level cleric of a good aligned god), this will only be done 1 time when the player first tries to pick up or touch the item
> > > - the **shield** forces you to experience the soft embrace of death, if the player fails 2 times (dc 15 and dc20 wis) they will die instantly, they can still be revived by a Resurrection spell or similar magic (not revivify)
> > > -  the **cloak** forces you into a murderous rage, if the player fails 2 times (dc 15 and dc 20 wis) they will go berserk until knocked unconscious, their alignment will also shift to evil and they must roll a d20 every day, on a 5 or less they fill fall into a rage for a number of minutes equal to the number rolled, this effect can be cleansed by a high level cleric of a good aligned god

2) the party exists the dungeon by sacrificing one of the cranium rats
3) the party comes into the bathhouse, see the scene 4 from [[10-prep]], however a lot of things changed, mortlock joined them, they went outside and saw flennis eating a corpse, they decided to attack flennis and almost had a TPK, during the battle all the other cultists were killed, as well as the other hostages/people around the bathhouse (circle of death)
4) the party kills flennis and briefly meets the emissary of the cult of tiamat that had kept out of the fight since his underlings were all killed, the emissary asked them about the crown but he failed his wisdom against rudra's deception, he continued down into the dungeon to look for the artifact (ignorant of the flaming fists that were arriving, they find him as he drank his potion of invisibility during the flennis fight)
5) the party meets the flaming fists that arrive at the scene, they are asked to come with them and faelyn is cuffed as she is the prime suspect (of balhazar gists robbery), morlock somewhat betrays the party after they tell the fists he was associated with the cult, he told the fists that **the party attacked the bathhouse**

---
