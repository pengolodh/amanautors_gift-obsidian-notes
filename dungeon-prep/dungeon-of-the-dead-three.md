the party will likely (or not) enter the dungeon, the [notes](https://5e.tools/adventure.html#bgdia,1,dungeon%20of%20the%20dead%20three)will give you the general layout
> I will do the following changes:
> - D3: south massage room
> >  => jabaz is a women, she is doing the whole massage business as a side job until she can afford to go to either waterdeep or candlekeep (she wants to learn magic, she hates her current job but likes some of the people she has met), her boss (Mortlock Vampathur) is an enormous asshole that pushes her to do things she does not want (not yet sexual, but close). Jabaz knows that Mortlock does strange things in the bathhouse during the evening, Mortlock warned her not to come close to the area of the bathhouse or the heap in the evening/night. Jabaz knows the location of a secret door in the other massage room, she is curious of what lies beyond it but has never dared enter the area, she will tell the party if they offer her a reason (and promise never to tell her boss she told them)
> 
> - D4: north massage room
> > => Qurmilah is an older lady (around 50), she does not care about the secret door, she knows its there but thinks its none of her business, if she massages a party member they need to make a con save cuz she got some strong forearms. Mortlock hired her because he knows she does not care about his activities (she worked as a prostitute in her earlier life) and will keep her nose out of his business, he mostly leaves er alone.
> > > ==> the secret door takes a dc 15 saving throw to find
> 
> - D6: room near altar (after secret door)
> > =>  the room is mostly empty except for a trail of entrails leading into the next room
>
> - D7: first shrine to bhaal
> > => Vistaak is standing in prayer in front of the alter, when the party enters he is carefully adorning the alter to the god of murder with a victims corpse. 
> >  If the players choose to watch instead of immediately attacking they will see him carefully placing the eyeballs of the remains into the sockets of the skull of bhaal.
> > Most of the body is placed on the alter underneath the skull, the entrails go around the skull, the head is placed in the mouth of bhaal.
> >  Vistaak will only acknowledge the players when he is done with his ritual. When the ritual is completed the eyes will lite up in red flames and fill the sockets. (see handout)
>
> - D8: Mural
> > => the mural is accented using dried, dark blood, the figure itself is carved into the stone. (see handout)
>
> - D9: Dead Three Doors
> > => the Doors each have a crude symbol carved into them, players need a dc 12 religion check to guess whose:
> > > - East Door: A back Fist holding shackles, the symbol of Bane
> > > - North Door: A skull surrounded by bloody knives, the symbol of Bhaal
> > > - South Door: A Grinning skull, its eyes are filled with a greenish light, the symbol of Myrkul
> 
> - D10: Necromite's Room
> > => three "corpses" surround the shrine, they lie unmoving, covered in dust. (the corpses are necromites)
> > if the party touches anything on the shrine, or go down to area **D11** they will rise to attack. The party can also see that they are alive with a dc 15 insight check.
>
> - D11:  Partially Collapsed Crypt
> > => replaced items and spells with different ones, see items/locations-specific/dungeon-of-dead-three/D11-collapsed-crypt (in foundry) for details
> 
> - D12: Bane's Altar
> > => mostly the same as in https://5e.tools/adventure.html#bgdia,1,d12.%20bane's%20altar,0 
> > > --> Klim is not evil, or a noble, he is a merchant from the steaps. He got kidnapped by the dead three followers about 3 days ago. He has nothing of note to share with the except the following:
> > > > -" Dont go into the cells, there is something nasty in there..." he could hear them groaning and scatching on the walls at night. (D19,20,21)
> > 
> > ==> like in the main notes, Hanging from Yignath's belt is an iron key ring with seven keys—one for the shackles above the altar, two for the shackles in [area D22](https://5e.tools/adventure.html#BGDIA,1,d22.%20torture%20chamber,0), and four that unlock the chests in [area D30](https://5e.tools/adventure.html#BGDIA,1,d30.%20tiamat's%20stolen%20treasure,0).
> 
> -D13: Morgue
> >=> flannis is a necromancer, she is incredibly fucked up and uses sewn together hands as tools to help her do vivisections on people. She is not really a follower of any of the cults, but she has an agreement with them to help them honor myrkul by processing the bodies for them.
> > > --> myrkul does not like this as her torturing people for sport untill they die is not really his thing, this has caused him to loose interest in this cult
> 
> -D16: Flooded Crypt
> > => the crypt is more bombastic, room is larger.
>
> -D18: Gas Buildup
> > => the gas here can be smelled,  dc 15 survival check
> 
> - D19: Partially collapsed crypt
> > => this area is used to keep live prisoners, mainly those kept by the cult to feed their rituals
> 
> - D20: Half plundered crypt
> > => the hidden bottom of the sacrophagus requires a dc 18 investigation roll,  the bag of beans is within its mouth
> 
> -D21:Zombie crypt
> > => there is loot in the human canibalism image crypt, namely: A 250gp worth gold riing
>
> -D22: torture chamber
> > - Efinnex  albor is the female, she was was attacked in the Lower City, knocked [unconscious](https://5e.tools/conditionsdiseases.html#unconscious_phb), and brought here to be tortured by the cult, but managed to catch the eye and was ultimately saved by the necromancer flennis. She now works as her (temporary) assistent and student though she is still imprisoned by the cult (she cannot leave).
> >  Before this:
> > > she distributed wine and spirits for the Oathoon patriar family of Baldur's Gate. Under interrogation, she gave her captors information regarding the Oathoons and their security arrangements
> > > -> she gave away this information before being tortured (out of fear), she caught the eye of flennis because the necromancer could sense her natural talent for necromancy.
> > > > ->- Oathoon—They imported a variety of quality wine and fine spirits into the city. (from [wiki](https://forgottenrealms.fandom.com/wiki/Nobility_of_Baldur%27s_Gate))
> >
> > - Vendetta Kress is a fist of bane that efinnex has been trying to convince (through seduction and spellcraft) to let her out of the dungeon, the fist has noticed this and is using this dynamic to convince Effinex to acquiesce to his demands (he teaches her the art of torturing and tortures her as well), its an abuse dynamic...
> >  --> they are currently in the torture chamber because vendetta wanted to "teach" effinax to better tolerate pain
> >
> > => Efinnex has heard Vendetta open some sort of door in the other room, but she has never seen it open herself
>
> - D23: Secret door and and sentry
> > -> the man is a black gauntlet of bane, he will call for aid when seeing the party, then retreat to area D24
> 
> - D24: Myrkul's rest
>   > -> a lone skeletal figures stands silently in the middle of this dusty chamber, on the walls are many shelves housing emaciated, dried up corpses. The room is covered in a silence spell, no sound or spell can be cast here. The figure will only attack the party when they disturb the rest of the corpses (by looting for example)
>   > > --> if they do loot the corpses they will find a dark green gem on each of them, it sparkles with a green light. this is called "the bonelords gift" and is given to his devout followers upon their final rest. It prevents undeath from affecting their corpses (the gem is a magical item that prevents undeath or rot form affecting a corpse)
>   
> - D25: Bane's rest
> > -> in the middle of this small room stands an opulent throne, surrounding that are many (5) small chests, each holding about 50GP and various gems totaling a value of 250GP, between the chests are many bedrolls. This is where the followers of bane slept. 
> > > --> the total value in gp is 500gp 
> 
> - D26: Bhaal's rest
>   > -> this cell like room is just big enough to fit a large sarcophagus, its lid is open and it is filled to the brim with blood, it stinks for good aligned characters and smells delicious for evil aligned characters. If an evil aligned character drinks from the blood they gain 1d12+4 hit points but also get a -1 charisma saving throw rebuff until long rest
>   
>   -D27: echoes of battle
>   > -> the party can hear the battle in area D29
>   
>   - D28: old cellar
>     > -> this old cellar holds a bunch of wandering skeletons that the party can see, behind those is a large trunk
>     > > => the trunk holds some essence of ether, a +1 dagger and a letter with the target the initiate must kill (the room is used to finish the training of a basic initiate of the cult)
>     
>  - D29: Ritual chamber
>    > -> mortlock has woken up and managed to kill 4 of the followers of bhaal, vaaz and 1 of his followers is facing a severely wounded but still formidable mortlock, when the party enters the room mortlock kills the final follower, leaving only vistaak
>    
>  - D30: Tiamat's stolen threasure
>    > -> This room contains 4 chests:
>    > > 1) contains 4500 sp and two crystal vails with gold stoppers, they both contain a potion of fire breath
>    > > 2) ten eye agates of 10 Gp each and 1250 sp
>    > > 3) a delicate porcelaine dragon mask, this item gives the wearer 3 charges of frost breath  and also contains 20pl pieces and 50 gp
>    > > 4) contains a bronze crown with five spires (250 gp). Each spire is shaped and painted to resemble one of the five kinds of chromatic dragons (black, blue, green, red, and white). The crown weighs 2½ pounds. wearing the crown gives you truesight
>    
>    - D31: stash of torches 
>      > -> name says it, its 5 torches in a box, if the party manages a dc15 investigation roll they will find a secret compartment filled with 4 small crystal bottles, these are 3 potions of invisibility and 1 potion of speed
>      
>    - D32: Stolen goods
>      > -> holds some basic necessities like food, water, spare daggers, etc. The room looks more like a storage closet then a treasure room.
>      
>     - D33: Covenant of the Dead Three
>       > -> this is 
>

---