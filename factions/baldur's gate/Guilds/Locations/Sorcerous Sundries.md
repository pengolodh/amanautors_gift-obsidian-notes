The bottom floor or [[Ramazith's Tower]], it is fairly large, at the center of the store is a magically created counter with a "projection" of Loroekon serving as personnel. It is a very stupid construct that has a few pre-programmed lines it can say.

The items of the shop are numerous, there are many shelves with magical artifacts and each artifact has a little paper slip next to them detailing the price of the item and a small (rather cryptic description) of what it does. If a person wishes to buy something they just take one of the paper slips and bring it to the counter, the item will be teleported to them after payment.

2 fire myrmidons guard the door of the shop, there is a large water elemental that goes around the shop periodically to scrub the floor.


![[sorcerers-sundries.webp]]
![[Sorcerous_Sundries_interior.webp]]
![[sorcerous-sundires-inter.jpg]]

---
#shop #mages