The main religions present of Baldur's Gate are:
> - [Gond](https://forgottenrealms.fandom.com/wiki/Gond), "Wonderbringer"
>   > -> he is worshiped in the [[The House of Gond]]
> - [Tymore](https://forgottenrealms.fandom.com/wiki/Tymora) , "lady luck"
> - [Umberlee](https://forgottenrealms.fandom.com/wiki/Umberlee), "the bitch queen"
> > -> she is worshipped in [[Water Queen's House]]
> - [Helm](https://forgottenrealms.fandom.com/wiki/Helm), "He of the Unsleeping Eyes"
> - [Lathander](https://forgottenrealms.fandom.com/wiki/Lathander), "Morninglord "
> - [Ilmater](https://forgottenrealms.fandom.com/wiki/Ilmater?so=search),  "the suffering god"
>   > -> their main temple is close to the wide, but they have many outposts in the Heap, where most of their activity lies, see [[Shrine of the Suffering]] 
> - [Ogma](https://forgottenrealms.fandom.com/wiki/Oghma), "The Binder of What is Known"
>  > -> it does not have a big temple in the city, however [[The Unrolling Scroll]] serves as its main shrine
> - The cults of the [Dead Three](https://forgottenrealms.fandom.com/wiki/Dead_Three?so=search) (see [[The Cult of the Dead Three]])

Each Church has their own vestige in the city, with Umberlee, Gond, Lathander and Helm having the biggest temples. 
Other greater deities like Sune have less of a footing in the city but of course have their own temples, some interesting ones are:
> - [Sune](https://forgottenrealms.fandom.com/wiki/Sune), "Lady Firehair" 
> - [Torm](https://forgottenrealms.fandom.com/wiki/Torm), "The True God"
> - ...

---
#religions 

---
