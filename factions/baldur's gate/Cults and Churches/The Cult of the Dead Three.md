The Cult of the dead three is an outlier, the 3 gods are not usually worshiped together as their mantles conflict, Bane fears Bhaal and both fear Myrkul, for example

-> The 3 gods are:
> - [[Bhaal]] ( see [wiki](https://forgottenrealms.fandom.com/wiki/Bhaal)) "Lord of Murder"
> - [[Bane]] (see [wiki](https://forgottenrealms.fandom.com/wiki/Bane?so=search)) "The Black Hand"
> - [[Myrkul]] (see [wiki](https://forgottenrealms.fandom.com/wiki/Myrkul)) "Lord of Bones"

![[A_mural_of_the_Dead_Three_in_1492_DR.webp]]

---
#religions #pc-backstory-relevant #rudra 

---
