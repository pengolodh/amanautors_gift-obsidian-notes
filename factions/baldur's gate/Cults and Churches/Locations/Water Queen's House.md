The oldest temple in Baldur's Gate, the Water Queen's House clings to its enormous pier, its stone walls trailing over the side and descending down beneath the waves and river mud. 

At the pier's tip, a huge fountain in the shape of a sinking ship sprays water high, reminding faithful of the price of failing to appease Umberlee. 

The intimidating Allandra Grey, a female priest, leads the temple's score of waveservants, most of them women widowed or orphaned by the sea.

---
#religions #temples

---
