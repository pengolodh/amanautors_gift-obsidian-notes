This vast workshop is the center of Gond's religion in Baldur's Gate. Every day, the anvils and worktables that fill the High House of Wonders ring with the clamor of hammer and saw. 

Ostensibly, Gond's priests offer healing and other magical services to anyone willing to pay. Rumors claim that the High House of Wonders maintains a secret testing facility in or just outside the city.

---
#religions #temples 

---
