This simple stone shrine to Ilmater, god of martyrs and patient endurance, stands in a small, quiet square. 

Here, poor Baldurians can come to receive free meals and enough coppers to pay their way through the city's gates, thanks to the ministrations of Brother Hodges, an halfling priest. Hodges is helping out in the tents at the moment, the party has not yet encountered him

> -> this is close to the tents that the church erected after the Heap burned down

---
#religions #shrines

---
