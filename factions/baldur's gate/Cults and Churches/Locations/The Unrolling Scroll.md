Built of white marble, the temple of Oghma stands out among the surrounding buildings. A wide reflecting pool rests in a deep basin in the center of the building, the rest of the building is stacked with books and scrolls.

Legend holds that bards and artists who study their own reflections in the basin for half a day behold a vision to inspire their next creation. As the Unrolling Scroll stands in the Upper City, non-residents of the district are evicted after sundown.

![[the-unrolled-scroll.jpg]]
> -> in recent times the unrolling scroll has moved away from being a shrine and has also become a place of learning


---
#religions #temples

---
