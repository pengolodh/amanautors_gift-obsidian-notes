> *"An idea has no heft but it can move mountains. An idea has no authority but it can dominate people. 
> An idea has no strength but it can push aside empires. 
> 
> Knowledge is the greatest tool of the mortal mind, outweighing anything made by mortal hands. Before anything can exist, the idea must exist."*

Was the neutral or unaligned greater god of inspiration, invention, and knowledge and the patron of bards in the Faerûnian pantheon.

The Lord of Knowledge was the leader of the Deities of Knowledge and Invention and as such was the most powerful god of knowledge in Faerûn.

The Binder of What is Known, or simply the Binder, was also a member of the Celtic pantheon as a neutral or neutral good intermediate deity of speech and writing, as well as patron of the arts and everything beautiful and creative in all genders.

Oghma's domain was nothing less than the whole of knowledge and thought, whether they were written down, spoken aloud, or even still remained in the mind, for the idea was the purest and highest incarnation of knowledge.

In the past (before the spellplague) Oghma was outwardly carefree and cheerful in manner, often with a quiet humor and a ready smile, the Binder was possessed of great wisdom and could be solemn and righteous when needed. He was legendary for his geniality and all beings apart from the most evil and hateful appreciated his fine singing voice and fabled musical talents. He possessed profound powers of persuasion, using his good looks, peerless charm, and rhetorical prowess to sway even his most fierce opposition toward his point of view, and he did so at any opportunity.

However, his critics—like fellow deities [[Lathander]] and Lliira, who were often radically opposed to conservatism—saw such oratory and charm as manipulative and narrow-minded.

But it was Oghma's burden to choose which ideas and thoughts would spread and which would fade away with their creator, so that all others would not be in jeopardy. Resting heavily in his heart were the experiences of millennia and hence in this duty he was serious, firmly protective, and deeply cautious.

He favored a doctrine of ideological conservatism and sticking strictly to the status quo, rather than risking a rogue idea disrupting the delicate balance of knowledge he had been preserving since history began

Yet after the Spellplague, something changed in Oghma. He was still the judge of ideas and innovations, choosing which would be remembered and forgotten, but the Lord of Knowledge could not stand the suppression of any original thought, regardless of whether the consequences were good or bad. 
He thrived on ideas, especially new ones, and the sharing of knowledge in all its forms,  this also mutated his shape, no longer did his avatar take a mortal form, as that would limit his expression.

He now resembles a mass of eyes, from him a thousand songs and instruments can be heard, in his shape all art is visible.

![[oghma-god-of-knowledge.jpg]]
> -> Oghma as he appeared before [[Theodore]] 


---
#gods #pc-backstory-relevant #theodore

---
