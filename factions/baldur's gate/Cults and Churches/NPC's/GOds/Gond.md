was the Faerûnian god of craft, smithing, and inventiveness. 

The Lord of All Smiths had many forms and names, and under all of them pushed for innovation and imaginativeness, sometimes to a dangerous degree, as a result of his short-sighted desire to create.

Gond was also known as Nebelun among gnomes (also those in [[High House of Wonders]]) of the Realms, who venerated him alongside the other Lords of the Golden Hills. The Six Secret Names of Gond were (in appropriate order): Aranlaerus, Balateng, Daerosdaeros, Klannauda, Mrangor, and Tattaba

The divine personality of the Wonderbringer was as varied as his desire to innovate was intense. He would at times act wholly disciplined and methodical, while at others become easily distracted. He was just as often insightful and accommodating as he was sardonic or condescending.

Gond remained ever-concerned with making real any theoretical concept or schematic, and held little concern for any complications or problems that arose therefrom. He would readily accept commissions from any unscrupulous patron, so long as compensation was substantial and he was given opportunity to create novel and innovative devices.
Gond was not anti-magic as some may think, but viewed magic as simply another means by which to craft new inventions.

![[Gond.png]]

---
#gods

---
