---

---
> *"I choose death...I can destroy your kingdom, [Bane](https://forgottenrealms.fandom.com/wiki/Bane "Bane"), by murdering your subjects, and I can starve your kingdom, [Myrkul](https://forgottenrealms.fandom.com/wiki/Myrkul "Myrkul"), by staying my hand."*

Was the widely feared **Lord of Murder**, the [Faerûnian](https://forgottenrealms.fandom.com/wiki/Faer%C3%BBnian_pantheon "Faerûnian pantheon") [god](https://forgottenrealms.fandom.com/wiki/God "God") of violence and ritualistic killing.

Bhaal only lived to hunt and kill, the presence of the living instilling in him an overpowering desire for death and destruction. He was at all times a cruel, violent and hateful being, though his behavior could vary from cold and calculating ruthlessness to a savage thirst for blood.


![[bhaal-2.jpg]]


---
#gods #religions #pc-backstory-relevant #rudra

---
