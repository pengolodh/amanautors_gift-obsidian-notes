was a [Faerûnian](https://forgottenrealms.fandom.com/wiki/Faer%C3%BBnian_pantheon "Faerûnian pantheon") [greater](https://forgottenrealms.fandom.com/wiki/Greater_deity "Greater deity") [god](https://forgottenrealms.fandom.com/wiki/God "God") with a vast portfolio including birth, renewal, spring and youth, as well as athletics, self-perfection, vitality and creativity. **The Morninglord** was symbolized by the rising sun, but rather than being the god of the sun itself, he was the god of dawn, which represented the potential of a new day. A god of hope and beginnings, Lathander's name was invoked at the start of new endeavors, whether sealing a new deal, or setting out on a new journey.

Despite being among the oldest of [Faerun](https://forgottenrealms.fandom.com/wiki/Faerun "Faerun")'s gods, Lathander nonetheless retained the cheery hopefulness of youth. His was an eternal optimism, a constant willingness to focus on hopes for the future rather than wallow in the failures of the present.

He was a doggedly determined god who encouraged proactive altruism and constant reevaluation of the old ways. his interests laid in vibrant life (regarding both birth and nature) and conversely urged the destruction of the corrupted mockeries of life that he saw the [undead](https://forgottenrealms.fandom.com/wiki/Undead "Undead") as.

Though liked for his many positive qualities, Lathander also had a reputation for displaying the flaws common to the young, such as zealotry, vanity, and excess.

Enthusiastically altruistic and only slightly vain, it was said by his critics that his aggressive do-gooder mentality often prevented him from taking more sensible courses of action. His headstrong conceit could blind him to the consequences of his actions as in his idealistic crusades he simply attacked directly and hoped for the best, ignoring the ramifications.

![[lathander.jpg]]
> -> Lathander as he appeared for [[Aster]] in [[Aster-dream]]

---
#gods #pc-backstory-relevant #aster

---
