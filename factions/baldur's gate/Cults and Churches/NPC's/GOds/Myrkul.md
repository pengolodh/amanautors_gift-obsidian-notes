> *"But I choose the dead, and by doing so I truly win… All things must die—even gods"*

Was a god of the dead and later a god of decay and exhaustion in the Faerûnian pantheon. He was originally a mortal man, a necromancer named Myrkul Bey al-Kursi, who ascended to godhood alongside Bhaal, the god of murder, and Bane, god of tyranny. His portfolio and home, the Bone Castle, were both usurped by the mortal Cyric and later passed on to the ascended Kelemvor.

However, as that which is dead can never truly die, Myrkul was worshiped as a god once more in the 15th century DR, the Reaper who brought more souls for the new Lord of the Dead to judge.

The face of the Lord of Bones, the white skull shrouded in a black cowl, was recognized across the Realms as the symbol of fear and death, the paragon of nightmare.


As a mortal, Myrkul Bey al-Kursi was described as a reticent and withdrawn individual. His thin frame and gangly limbs were hidden behind dark robes, and he spoke in a high-pitched but soft voice.

It was very important to Myrkul that Faerûnians always kept him in the back of their mind. He had mastered the skill of sparking unease and fear amongst mortals through every action or mere word and never missed an opportunity to remind the world that he was waiting for them all.

![[myrkul.jpg]]

---
#gods #pc-backstory-relevant #rudra

---