> *"..I choose to rule for all eternity as the ultimate tyrant. I can induce hatred and strife at my whim, and all will bow down before me while in my kingdom."*

Was the Faerunian god of tyrannical oppression, terror, and hate, known across Faerûn as the face of pure evil through malevolent despotism. From his dread plane of [Banehold](https://forgottenrealms.fandom.com/wiki/Banehold "Banehold"), The Black Hand acted indirectly through worshipers and other agents to achieve his ultimate plan to achieve total domination of all Faerun.

Bane was a wholly malevolent and brutal deity that distanced himself from mere mortals, preferring to reign over his followers from afar. He savored the terror he instilled in others and the hatred that formed in mortals' hearts, utilizing this strife to gain greater control over the Realms.

He embodied the principles of ambition and control and believed that the strong had not only the right, but the duty, to rule over the weak. He favored those with drive and courage, often aiding those who sought to establish order or conquer new territories. While he was seen as a savior in some regions, his teachings also led to dictatorships and the promotion of slavery.

While Bane believed himself the rightful ruler of all the planes and could not tolerate subservience to anyone, the god was willing, unlike many evil deities, to work with others if it served his interests and the god formed multiple alliances. 

Most notable perhaps was his alliance with [[Myrkul]], which stretched back to when both were mortals and which continued until both of their deaths during the Time of Troubles. Bane also had working relations with the gods [Loviatar](https://forgottenrealms.fandom.com/wiki/Loviatar "Loviatar"), [Talona](https://forgottenrealms.fandom.com/wiki/Talona "Talona"), and [Mask](https://forgottenrealms.fandom.com/wiki/Mask "Mask"). 

When Bane returned to life in 1372 DR, he quickly went about reforging these alliances, primarily by reasserting their fears of him.

Besides allies, Bane also had servants such as [[Bhaal]] and his own son [Iyachtu Xvim](https://forgottenrealms.fandom.com/wiki/Iyachtu_Xvim "Iyachtu Xvim") during his first life as a god, along with [Abbathor](https://forgottenrealms.fandom.com/wiki/Abbathor "Abbathor"), [Maglubiyet](https://forgottenrealms.fandom.com/wiki/Maglubiyet "Maglubiyet"), [Hruggek](https://forgottenrealms.fandom.com/wiki/Hruggek "Hruggek"), and [Tiamat](https://forgottenrealms.fandom.com/wiki/Tiamat "Tiamat") during his second tenure. [Malar](https://forgottenrealms.fandom.com/wiki/Malar "Malar") was also known to work along with Bane at times.

Bane was the former lover of the goddess [Kiputytto](https://forgottenrealms.fandom.com/wiki/Kiputytto "Kiputytto"). He attempted to steal her divine power during their amorous encounters, but was ultimately unsuccessful.

But as numerable as his allies, Bane had many enemies as well. For a time, Bane's most hated foe was the goddess of magic, [Mystra](https://forgottenrealms.fandom.com/wiki/Mystra "Mystra"), whose power he coveted. 

Since his return, however, Bane's greatest foes were Cyric who stole from him many of his worshipers and the [Zhentarim](https://forgottenrealms.fandom.com/wiki/Zhentarim "Zhentarim"), and the [Triad](https://forgottenrealms.fandom.com/wiki/Triad "Triad"), particularly its formerly junior but now senior member Torm, who was the being responsible for Bane's first death. Bane was also enemies with the **gods [Amaunator](https://forgottenrealms.fandom.com/wiki/Amaunator "Amaunator") and [Oghma](https://forgottenrealms.fandom.com/wiki/Oghma "Oghma")** and called [Helm](https://forgottenrealms.fandom.com/wiki/Helm "Helm") and [Midnight](https://forgottenrealms.fandom.com/wiki/Midnight "Midnight") enemies as well when both gods lived.

![[bane.jpg]]

---
#gods #religions #pc-backstory-relevant #rudra

---
